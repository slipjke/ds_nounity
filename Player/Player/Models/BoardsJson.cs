﻿using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Player.Models
{
    public class BoardUrl
    {
        public string url { get; set; }
        public string md5 { get; set; }
        public string mime_type { get; set; }
    }

    public class BoardCell
    {
        public int sliderInterval { get; set; }
        public string resizedMode { get; set;}
        public List<BoardUrl> urls { get; set; }
        public int sortOrder { get; set; }
        public string type { get; set; }
    }
    public class BoardItem
    {
        public List<BoardCell> cells;
        public string id { get; set; }
        public string name { get; set; }
        public string layout { get; set; }
        public List<string> ItemsList { get; set; }
        public BoardItem(string id, string name, List<string> items)
        {
            this.id = id;
            this.name = name;
            ItemsList = items;
        }
        public static async Task<BoardItem> ParceBoardJson(string path)
        {
            JObject json = JObject.Parse(File.ReadAllText(path));
            return JsonConvert.DeserializeObject<BoardItem>(json.ToString());
        }
    }

}
