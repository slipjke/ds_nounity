﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player._PathManager;
using System.IO;

namespace Player._FileControl
{
    public static class Log
    {
        public async static Task CreateNewLogFile()
        {
            if (File.Exists(PathManager.logFilePath))
            {
                await Task.Run(() => File.Delete(PathManager.logFilePath));
            }
            await Task.Run(() => File.Create(PathManager.logFilePath));
        }
        public async static Task Record(string massage)
        {
            StreamWriter sw = new StreamWriter(PathManager.logFilePath, true);
            sw.WriteLineAsync(massage);
            sw.Close();
        }
    }
}
