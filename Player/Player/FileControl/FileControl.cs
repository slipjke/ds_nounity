﻿using System.Net.Http;
using Player._Network;
using Player._PathManager;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System;
using Player.Models;
using System.Net;
using System.Collections.Generic;

namespace Player._FileControl
{
    public class FileControl
    {
        private Network network;
        public List<BoardItem> BoardListFromServer;
        public FileControl(Network _network)
        {
            network = _network;
        }
        public async Task<bool> CreateMainFolders()
        {
            try
            {
                if (!Directory.Exists(PathManager.mainFolderPath))
                {
                    await Task.Run(() => Directory.CreateDirectory(PathManager.dataFolderPath));
                }
                if (!Directory.Exists(PathManager.boardsFolderPath))
                {
                    await Task.Run(() => Directory.CreateDirectory(PathManager.boardsFolderPath));
                }
                return true;
            }
            catch(Exception e)
            {
                await Log.Record("Не удалось создать основные папки, ошибка:" + e.Message);
                return false;
            }
        }
        public async Task<bool> UpdateBoardData(BoardItem board)
        {
            try
            {
                await network.Assign(board.id);
                var json = await network.GetJsonOfItem();
                BoardItem updatedItem = JsonConvert.DeserializeObject<BoardItem>(json["digital_signage"].ToString());
                string boardItemPath = Path.Combine(PathManager.boardsFolderPath, updatedItem.name);

                foreach (var cell in updatedItem.cells ?? Enumerable.Empty<BoardCell>())
                {
                    foreach (var url in cell.urls ?? Enumerable.Empty<BoardUrl>())
                    {
                        string FileName = await GetFileName(url.url);
                        if (!File.Exists(Path.Combine(boardItemPath, FileName)))
                        {
                            await DownloadData(new Uri(PathManager.defaultServer + url.url), Path.Combine(boardItemPath, FileName));
                        }
                    }
                }
                return true;
            }
            catch(Exception e)
            {
                await Log.Record("Не удалось обновить борду, ошибка:" + e.Message);
                return false;
            }
        }
        public async Task<string> GetFileName(string fileName)
        {
            try
            {
                for (int i = fileName.Length - 1; i != 0; i--)
                {
                    if (fileName[i] == '/' || fileName[i] == '\\')
                    {
                        return fileName.Substring(i + 1, fileName.Length - i - 1);
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
        public async Task<bool> CheckBoardContent(BoardItem board)
        {
            try
            {
                string boardItemPath = Path.Combine(PathManager.boardsFolderPath, board.name);
                if (!Directory.Exists(boardItemPath)) await Task.Run(() => Directory.CreateDirectory(boardItemPath));
                if (board.ItemsList == null || board.ItemsList.Count == 0) return false;

                foreach (var name in board.ItemsList)
                {
                    if (!File.Exists(Path.Combine(boardItemPath, name)))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                await Log.Record("Не удалось проверить локальный контент, ошибка:" + e.Message);
                return false;
            }
        }
        public async Task<List<BoardItem>> GetBoardsListFromServer()
        {
            try
            {
                if (BoardListFromServer != null) return BoardListFromServer;

                BoardListFromServer = await network.GetBoardsList();
                return BoardListFromServer;
            }
            catch
            {
                return new List<BoardItem>();
            }
        }
        public async Task<BoardItem> GetLastActiveBoard()
        {
            try
            {
                var json = JObject.Parse(File.ReadAllText(PathManager.activeBoardConfigPath));
                BoardItem board = JsonConvert.DeserializeObject<BoardItem>(json.ToString());

                board.ItemsList = new List<string>();

                foreach (var cell in board.cells ?? Enumerable.Empty<BoardCell>())
                {
                    foreach (var url in cell.urls ?? Enumerable.Empty<BoardUrl>())
                    {
                        string FileName = await GetFileName(url.url);
                        board.ItemsList.Add(FileName);
                    }
                }

                return board;
            }
            catch
            {
                return default;
            }
        }
        public async Task<bool> ChangeActiveBoard(BoardItem board)
        {
            try
            {
                if (!File.Exists(PathManager.activeBoardConfigPath))
                {
                    await Task.Run(() => File.Create(PathManager.activeBoardConfigPath).Close());
                }

                await network.Assign(board.id);
                var json = await network.GetJsonOfItem();
                File.WriteAllText(PathManager.activeBoardConfigPath, JsonConvert.SerializeObject(json["digital_signage"]));

                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> CheckToken()
        {
            try
            {
                var json = JObject.Parse(File.ReadAllText(Path.Combine(PathManager.dataFolderPath, "token.json")));
                string token = json["token"].ToString();
                string deviceToken = json["deviceToken"].ToString();
                HttpClient tmpclient = new HttpClient();

                tmpclient.DefaultRequestHeaders.Add("token", token);
                tmpclient.DefaultRequestHeaders.Add("deviceToken", deviceToken);

                var response = await tmpclient.GetAsync(PathManager.server + "list");

                response.EnsureSuccessStatusCode();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    bool isTokenExist = false;
                    foreach(var header in Network.client.DefaultRequestHeaders)
                    {
                        if (header.Key == "token")
                        {
                            isTokenExist = true;
                            break;
                        }
                    }
                    if (!isTokenExist)
                    {
                        Network.client.DefaultRequestHeaders.Add("token", token);
                        Network.client.DefaultRequestHeaders.Add("deviceToken", deviceToken);
                    }
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        private async Task<bool> DownloadData(Uri url, string path)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    byte[] fileBytes = await client.GetByteArrayAsync(url);
                    await Task.Run(() => File.WriteAllBytes(path, fileBytes));
                }
                return true;
            }
            catch (Exception e)
            {
                await Log.Record("Не удалось скачать файл, ошибка:" + e.Message);
                return false;
            }
        }
        
    }
}