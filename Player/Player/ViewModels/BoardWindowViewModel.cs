﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Player.Models;
using Nito.AsyncEx;
using System.ComponentModel;
using System;
using System.Collections.Generic;

namespace Player.ViewModels
{
    public class BoardWindowViewModel : ViewModelBase
    {
        public static BoardItem ChosenBoard; 

        //Property
        public event PropertyChangedEventHandler PropertyChanged;
        public INotifyTaskCompletion InitializationNotifier { get; private set; }
        //Data
        private ObservableCollection<BoardItem> _boardItems;
        public ObservableCollection<BoardItem> BoardItems
        {
            get => _boardItems;
            set
            {
                if (value != null)
                {
                    _boardItems = value;
                    OnPropertyChanged();
                }
            }
        }
        private BoardItem _selectedItem;
        public BoardItem selectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                OnPropertyChanged();
                ChosenBoard = value;
            }
        }
        //Visualisation
        private string _gridRows;
        public string GridRows
        {
            get => _gridRows;
            set
            {
                _gridRows = value;
                OnPropertyChanged();
            }
        }

        //Functions
        public BoardWindowViewModel(List<BoardItem> Items)
        {
            ChosenBoard = default;
            InitializationNotifier = NotifyTaskCompletion.Create(InitializeAsync(Items));
        }
        private async Task InitializeAsync(List<BoardItem> Items)
        {
            BoardItems = new ObservableCollection<BoardItem>(Items);
            GridRows = Math.Ceiling((double)BoardItems.Count / 3).ToString();
        }
    }
}
