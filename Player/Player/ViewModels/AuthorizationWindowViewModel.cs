﻿using System.Threading.Tasks;
using Nito.AsyncEx;
using ReactiveUI;
using System.Reactive;

namespace Player.ViewModels
{
    public class AuthorizationWindowViewModel : ViewModelBase
    {
        public static bool isEntryConfirm = false;

        //Property
        public INotifyTaskCompletion InitializationNotifier { get; private set; }
        //Values
        private ReactiveCommand<Unit, Unit> AuthButton { get; }
        private string login;
        private string pass;
        private string loginTextBox
        {
            get => login;
            set
            {
                if (value != null)
                {
                    login = value;
                    OnPropertyChanged();
                }
            }
        }
        private string passTextBox
        {
            get => pass;
            set
            {
                if (value != null)
                {
                    pass = value;
                    OnPropertyChanged();
                }
            }
        }
        //Functions
        public AuthorizationWindowViewModel()
        {
            InitializationNotifier = NotifyTaskCompletion.Create(InitializeAsync());
            AuthButton = ReactiveCommand.Create(Entry);
        }

        private async Task InitializeAsync()
        {
        }
        private async void Entry()
        {
            if (!await MainWindowViewModel.network.GetToken(loginTextBox, passTextBox))
            {
                loginTextBox = "";
                passTextBox = "";
            }
            else isEntryConfirm = true;
        }

    }
}
