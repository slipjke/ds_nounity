using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Player.Models;
using Player._Network;
using Player._FileControl;
using Player._PathManager;
using Nito.AsyncEx;
using System.ComponentModel;
using ReactiveUI;
using System.Reactive;
using LibVLCSharp.Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Player.Views;
using System.IO;
using System.Linq;

namespace Player.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        const string S_TRUE = "True";
        const string S_FALSE = "False";

        //FileControl
        private FileControl fileControl;

        //Network
        public static Network network;

        //Property
        public event PropertyChangedEventHandler PropertyChanged;
        public INotifyTaskCompletion InitializationNotifier { get; private set; }

        //Window
        private string _progressBar = S_TRUE;
        private string ProgressBar
        {
            get => _progressBar;
            set
            {
                if (value != null)
                {
                    _progressBar = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _menuStackPanel = S_FALSE;
        private string MenuStackPanel
        {
            get => _menuStackPanel;
            set
            {
                if (value != null)
                {
                    _menuStackPanel = value;
                    OnPropertyChanged();
                }
            }
        }
        private ReactiveCommand<Unit, Unit> BoardChooseButton { get; }
        private ReactiveCommand<Unit, Unit> SettingsButton { get; }
        private ReactiveCommand<Unit, Unit> QuitButton { get; }

        //Visualisation
        private int _contentWidth;
        private int contentWidth
        {
            get => _contentWidth;
            set
            {
                if (value != null)
                {
                    _contentWidth = value;
                    OnPropertyChanged();
                }
            }
        }
        private int _contentHeight;
        private int contentHeight
        {
            get => _contentHeight;
            set
            {
                if (value != null)
                {
                    _contentHeight = value;
                    OnPropertyChanged();
                }
            }
        }
        
        public static string _videoVisible = S_FALSE;
        private string VideoVisible
        {
            get => _videoVisible;
            set
            {
                if (value != null)
                {
                    _videoVisible = value;
                    OnPropertyChanged();
                }
            }
        }
        private readonly LibVLC _libVlc;
        public MediaPlayer _mediaPlayer;
        public MediaPlayer mediaPlayer
        {
            get => _mediaPlayer;
            set
            {
                if (value != null)
                {
                    _mediaPlayer = value;
                    OnPropertyChanged();
                }
            }
        }

        //Functions
        public MainWindowViewModel()
        {
            _libVlc = new LibVLC();

            InitializationNotifier = NotifyTaskCompletion.Create(InitializeAsync());
            BoardChooseButton = ReactiveCommand.Create(StartBoards);
            QuitButton = ReactiveCommand.Create(Quit);
        }
        private async Task InitializeAsync()
        {
            ProgressBar = S_TRUE;
            await Init();
            await Authorization(); 
            if(!await StartLastActiveBoard()) StartBoards();
        }
        private async Task OpenMenu()
        {
            ProgressBar = S_FALSE;
            MenuStackPanel = S_TRUE;
            while(MenuStackPanel != S_FALSE)
            {
                await Task.Delay(500);
            }
        }
        private async Task Init()
        {
            network = new Network();
            fileControl = new FileControl(network);
            mediaPlayer = new MediaPlayer(_libVlc);

            await Log.CreateNewLogFile();
            await fileControl.CreateMainFolders();
        }
        private async Task Authorization()
        {
            if (!await fileControl.CheckToken())
            {
                AuthorizationWindow authWindow = new AuthorizationWindow
                {
                    DataContext = new AuthorizationWindowViewModel(),
                };
                authWindow.Show();
                /*authWindow.Closing += (s, e) =>
                {
                    e.Cancel = true;
                };*/
                while (!AuthorizationWindowViewModel.isEntryConfirm && authWindow.IsVisible)
                {
                    await Task.Delay(500);
                }

                authWindow.Close();
            }
        }
        private async Task RunBoardChoicePanel(List<BoardItem> Items)
        {
            MenuStackPanel = S_FALSE;
            BoardWindow boardWindow = new BoardWindow
            {
                DataContext = new BoardWindowViewModel(Items),
            };

            boardWindow.Show();

            while (BoardWindowViewModel.ChosenBoard == default && boardWindow.IsVisible)
            {
                await Task.Delay(500);
            }

            boardWindow.Close();

            BoardItem board = BoardWindowViewModel.ChosenBoard;
            
            if (board != default)
            {
                await fileControl.ChangeActiveBoard(board);
                board = await fileControl.GetLastActiveBoard();

                if (await PrepareBoardData(board)) await Visualisation(board);
            }
            else
            {
                await OpenMenu();
            }
        }
        private async void StartBoards()
        {
            List<BoardItem> boards = await fileControl.GetBoardsListFromServer(); 

            if (boards.Count == 0){
                await Inform("��� ���� ��� �������");
                await InitializeAsync();
                return;
            }
            await RunBoardChoicePanel(boards);
        }
        private async Task<bool> PrepareBoardData(BoardItem board)
        {
            ProgressBar = S_TRUE;
            if (!await TryRunBoard(board))
            {
                await Inform("������ ��� ������� ����asdasd ���");
                await OpenMenu();
                return false;
            }
            ProgressBar = S_FALSE;
            return true;
             
        }
        private async Task<bool> TryRunBoard(BoardItem board)
        {
            bool contentAvailable = true;
            if (!await fileControl.CheckBoardContent(board))
            {
                contentAvailable = await fileControl.UpdateBoardData(board);
            }
            if (contentAvailable)
            {
                return true;
            }
            return false;
        }
        private async Task Visualisation(BoardItem board)
        {
            VideoVisible = S_TRUE;

            foreach (var cell in board.cells ?? Enumerable.Empty<BoardCell>())
            {
                foreach (var url in cell.urls ?? Enumerable.Empty<BoardUrl>())
                {
                    string FileName = await fileControl.GetFileName(url.url);
                    await Play(Path.Combine(PathManager.boardsFolderPath, $"{ board.name}\\{ FileName}"), cell.resizedMode);
                    await Task.Delay(200);

                    while (mediaPlayer.IsPlaying && _videoVisible == S_TRUE)
                    {
                        await Task.Delay(100);
                    }

                    if (_videoVisible == S_FALSE)
                    {
                        VideoVisible = S_FALSE;
                        mediaPlayer.Stop();
                        ProgressBar = S_TRUE;
                        if (!await fileControl.CheckToken())
                        {
                            await InitializeAsync();
                            return;
                        }
                        await OpenMenu();
                        return;
                    }
                }
            }
            VideoVisible = S_FALSE;
            if(board.ItemsList.Count == 0)
            {
                await Inform("������ ��� ������� ����� ���");
                await OpenMenu();
            }
            else Visualisation(board);
        }
        private async Task<bool> StartLastActiveBoard()
        {
            BoardItem board = await fileControl.GetLastActiveBoard();
            if (board != default)
            {
                if (await fileControl.CheckToken())
                {
                    if (await PrepareBoardData(board))
                    {
                        await fileControl.ChangeActiveBoard(board);
                        await Visualisation(board);
                    }
                }
                else Visualisation(board);
                return true;
            }
            return false;
        }
        private async Task Play(string path, string resizedMode)
        {
            Media media = new Media(_libVlc, path);
            int media_width = MainWindow.WindowWidth, media_height = MainWindow.WindowHeight;

            switch (resizedMode)
            {
                case "centerCrop":
                    if (media_width < media_height || (MainWindow.WindowWidth < MainWindow.WindowHeight && media_width == media_height))
                    {
                        contentHeight = (int)(MainWindow.WindowWidth * (double)media_height / media_width);
                        contentWidth = MainWindow.WindowWidth;
                    }
                    else if (media_width > media_height || (MainWindow.WindowWidth > MainWindow.WindowHeight && media_width == media_height))
                    {
                        contentHeight = (int)(MainWindow.WindowHeight * (double)media_width / media_height);
                        contentWidth = MainWindow.WindowHeight;
                    }
                    else
                    {
                        contentHeight = MainWindow.WindowHeight;
                        contentWidth = MainWindow.WindowWidth;
                    }
                    break;
                case "original":
                    contentHeight = media_height;
                    contentWidth = media_width;
                    break;
                default:
                    contentHeight = MainWindow.WindowHeight;
                    contentWidth = MainWindow.WindowWidth;
                    break;
            }


            await Task.Run(() => mediaPlayer.Play(media));

            
        }
        private void Quit()
        {
            System.Environment.Exit(0);
        }
        private async Task Inform(string message)
        {
            NotificationWindow notificationWindow = new NotificationWindow
            {
                DataContext = new NotificationWindowViewModel(message),
            };
            notificationWindow.Show();
            while (notificationWindow.IsVisible)
            {
                await Task.Delay(500);
            }
            await Log.Record(message);
        }
    }
}
