﻿
namespace Player.ViewModels
{
    public class NotificationWindowViewModel : ViewModelBase
    {
        public string _notificationText;
        public string NotificationText
        {
            get => _notificationText;
            set
            {
                _notificationText = value;
                OnPropertyChanged();
            }
        }
        public NotificationWindowViewModel(string message)
        {
            NotificationText = message;
        }
    }
}
