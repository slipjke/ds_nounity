﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using Player.Models;
using Player._PathManager;
namespace Player._Network
{
    public class Network
    {
        public static readonly HttpClient client = new HttpClient();
        public async Task<bool> GetToken(string email, string password)
        {
            try
            {
                string postData = "email=" + email + "&password=" + password + "&serialNumber=123&name=asd";
                StringContent content = new StringContent(postData, System.Text.Encoding.UTF8, "application/form-data");
                HttpResponseMessage response = await client.PostAsync(PathManager.server + "login", content).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                string token = JObject.Parse(responseBody)["token"].ToString();
                string deviceToken = JObject.Parse(responseBody)["deviceToken"].ToString();

                Network.client.DefaultRequestHeaders.Add("token", token);
                Network.client.DefaultRequestHeaders.Add("deviceToken", deviceToken);

                await CreateTokenJson(token, deviceToken);
                
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else return false;
            }
            catch
            {
                return false;
            }
        }
        public async Task<List<BoardItem>> GetBoardsList()
        {
            try
            {
                List<BoardItem> items = new List<BoardItem>();

                var response = await client.GetAsync(PathManager.server + "list");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                bool isIdExist;

                var json = JObject.Parse(responseBody);
                foreach (var item in json["data"])
                {
                    isIdExist = false;    
                    foreach(var element in items)
                    {
                        if(element.id == item["id"].ToString()) { isIdExist = true; break; }
                    }
                    if(!isIdExist) items.Add(new BoardItem(item["id"].ToString(), item["name"].ToString(), new List<string>()));
                }

                if (response.StatusCode == HttpStatusCode.OK) return items;
                else return new List<BoardItem>();
            }
            catch
            {
                return new List<BoardItem>();
            }
        }
        public async Task<bool> Assign(string id)
        {
            try
            {
                var response = await client.GetAsync(PathManager.server + "assign?id=" + id);
                response.EnsureSuccessStatusCode();
                if (response.StatusCode == HttpStatusCode.OK) return true;
                else return false;
            }
            catch
            {
                return false;
            }
        }
        public async Task<JObject> GetJsonOfItem()
        {
            try
            {
                var response = await client.GetAsync(PathManager.server + "get");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                var json = JObject.Parse(responseBody);

                if (response.StatusCode == HttpStatusCode.OK) return json;
                else return null;
            }
            catch
            {
                return null;
            }
        }
        private async Task<bool> CreateTokenJson(string token, string deviceToken)
        {
            try
            {
                if (File.Exists(PathManager.tokenPath))
                {
                    using (StreamWriter writer = new StreamWriter(PathManager.tokenPath))
                    {
                        await writer.WriteAsync($"{{\"token\":\"{token}\", \"deviceToken\":\"{deviceToken}\",}}");
                        writer.Close();
                    }
                }
                else
                {
                    await Task.Run(() => File.Create(PathManager.tokenPath).Close());
                    using (StreamWriter writer = new StreamWriter(PathManager.tokenPath))
                    {
                        await writer.WriteAsync($"{{\"token\":\"{token}\", \"deviceToken\":\"{deviceToken}\",}}");
                        writer.Close();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}