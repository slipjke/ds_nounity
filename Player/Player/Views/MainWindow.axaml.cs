using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Input;
using Player.ViewModels;

namespace Player.Views
{
    partial class MainWindow : Window
    {
        public static int WindowWidth;
        public static int WindowHeight;

        public MainWindow()
        {
            InitializeComponent();
            AddHandler(KeyDownEvent, KeyDown, handledEventsToo: true);
#if DEBUG
            this.AttachDevTools();
#endif
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            WindowWidth = (int)this.Screens.All[0].WorkingArea.Width;
            WindowHeight = (int)this.Screens.All[0].WorkingArea.Height;
        }
        private void KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Escape) MainWindowViewModel._videoVisible = "False";
        }
    }
}
