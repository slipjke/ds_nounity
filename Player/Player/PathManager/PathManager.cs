﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Player._PathManager
{
    public static class PathManager
    {
        public static string defaultServer          =  "https://dev2.i-desk.pro";
        public static string server                 =  "https://dev2.i-desk.pro/api/v1/digital-signage/";
        public static string mainFolderPath         =  $"C:\\Users\\{Environment.UserName}\\AppData\\Local\\IDESK_TEST\\";
        public static string logFilePath            =  Path.Combine(mainFolderPath, "log.txt");
        public static string dataFolderPath         =  Path.Combine(mainFolderPath, "Data\\");
        public static string tokenPath              =  Path.Combine(dataFolderPath + "token.json");
        public static string boardsFolderPath       =  Path.Combine(dataFolderPath, "Boards\\");
        public static string activeBoardConfigPath  =  Path.Combine(dataFolderPath, "active_board.json");
    }
}
